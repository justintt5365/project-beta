# CarCar


## Contents

- [CarCar](#carcar)
  - [Contents](#contents)
  - [Team](#team)
  - [Instructions](#instructions)
  - [Design](#design)
  - [Service microservice](#service-microservice)
  - [Sales microservice](#sales-microservice)
      - [Models/Attributes:](#modelsattributes)
      - [Integrations:](#integrations)

---
## Team

* Tiffany Ameral - service
* Justin Thomas - sales

---
## Instructions

1. git fork/clone https://gitlab.com/justintt5365/microservice-two-shot.git
2. either open a terminal & CD into project or use the terminal in your IDE
3. run the following commands:
    ```docker volume create beta-data```
    ```docker-compose build```
    ```docker-compose up```
4. once all containers are up and running (react-1 my take a few minutes) you can start navigating the website by going to http://localhost:3000/
5. A lot of objects depend on each other, in order to get each form and list working on a fresh database, follow the following instructions:
   1. Inventory → Add a manufacturer
   2. Inventory → Add a vehicle model
   3. Inventory → Add an automobile
   4. Sales → Add a customer
   5. Sales → Add a sales person
   6. Sales → Add sales record form
   7. Service → Add Technician
   8. Service → Add Service
    
---
## Design
![planning diagram](Project-Beta-Diagram.png)

---
## Service microservice

Explain your models and integration with the inventory
microservice, here.

#### Models/Attributes:
**AutomobileVO**
-vin
**Technician**
-name
-employee_ID

**ServiceAppoitment**
-vin
-customer
-technician
-appt_date
-reason
-completed
-purchased 

#### Integrations:
Create get automobile function to poll for vin data from automobiles in inventory.

Create Encoders each model

Create API api_list_appts to GET and POST service appoitments

Create API api_service_detail to GET DELETE specific appoitment data

Create API api_technicians people to GET and POST technicans

Create API api_technician to GET and DELETE specific technican data

---
## Sales microservice

#### Models/Attributes:
**automobileVO**
- vin

**Sales Person**
- name 
- employee ID

**Potential Customer**
- name
- address
- phone number

**Sale Record**
- VO automobile
- sales person(ForeignKey)
- customer(ForeignKey)
- price

#### Integrations:
Created get automobile function to poll for vin data from automobiles in inventory.

Create Encoders each model

Create API customers to GET and POST customers <br>
End point: http://localhost:8090/api/customers

Create API customer to GET DELETE specific customer data <br>
End point: http://localhost:8090/api/customers/{pk}

Create API sales people to GET and POST sales people  <br>
End point: http://localhost:8090/api/sales/

Create API sales person to GET and DELETE specific sales person data  <br>
End point: http://localhost:8090/api/sales/{pk}

Create API sales records to GET and POST sales records  <br>
End point: http://localhost:8090/api/records/

Create API sales record to GET and DELETE specific sales record  <br>
End Point: http://localhost:8090/api/records/{pk}


