import React from 'react';

class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            address: '',
            phoneNumber: '',
          };
          this.handleNameChange = this.handleNameChange.bind(this);
          this.handleAddressChange = this.handleAddressChange.bind(this);
          this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
    }

    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({phoneNumber: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log({...this.state})
        const data = {...this.state};
        data.phone_number = data.phoneNumber;
        delete data.phoneNumber;

        const salesUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            const cleared = {
                name: '',
                address: '',
                phoneNumber: '',
                bool: true,
            };
            this.setState(cleared);
        }
    }

    render() {
      let formClass = ''
      let alertClass = 'alert alert-success d-none mb-0'
      if (this.state.bool === true) {
          formClass = 'card shadow d-none'
          alertClass = 'alert alert-success mb-0'
      }
      return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <form onSubmit={this.handleSubmit} id="create-salesPerson-form" className={formClass}>
            <h1>Add a new customer</h1>
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleAddressChange} value={this.state.address} placeholder="address" required type="text" name="address" id="address" className="form-control"/>
                <label htmlFor="starts">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePhoneNumberChange} value={this.state.phoneNumber} placeholder="phoneNumber" required type="tel" name="phoneNumber" id="phoneNumber" className="form-control"/>
                <label htmlFor="starts">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={alertClass} id="success-message">
              <p>Customer has been created!</p>
            </div>
          </div>
        </div>
      </div>
      );
    }
  }

export default CustomerForm;