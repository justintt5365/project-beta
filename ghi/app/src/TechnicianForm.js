import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_ID: "",
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleemployeeIDChange = this.handleemployeeIDChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleemployeeIDChange(event) {
        const value = event.target.value;
        this.setState({ employee_ID: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log("this.state:****", { ...this.state })
        const data = { ...this.state };
        // data.employee_ID = data.employeeID;
        // delete data.employeeID;

        const techUrl = 'http://localhost:8080/api/technician/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            const cleared = {
                name: "",
                employee_ID: "",
            };
            this.setState(cleared);
        }
    }

render() {
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new technician</h1>
                    <form onSubmit={this.handleSubmit} id="create-salesPerson-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleemployeeIDChange} value={this.state.employee_ID} placeholder="employeeID" required type="text" name="employeeID" id="employeeID" className="form-control" />
                            <label htmlFor="starts">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
}

export default TechnicianForm;
