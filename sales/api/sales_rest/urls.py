from django.urls import path

from .views import (
    api_customers,
    api_customer,
    api_sales_people,
    api_sales_person,
    api_sales_record,
    api_sales_records,
    list_sales_person_records,
)

urlpatterns = [
    path(
        "customers/",
        api_customers,
        name="api_customers",
    ),
    path(
        "customers/<int:pk>",
        api_customer,
        name="api_customer",
    ),
    path(
        "sales/",
        api_sales_people,
        name="api_sales_people",
    ),
    path(
        "sales/<int:pk>",
        api_sales_person,
        name="api_sales_person",
    ),
    path(
        "records/",
        api_sales_record,
        name="api_sales_record",
    ),
    path(
        "records/<int:pk>",
        api_sales_records,
        name="api_sales_records",
    ),
    path(
        "sales/<int:employee_ID>/records/",
        list_sales_person_records,
        name="list_sales_person_records",
    ),
]