from urllib import response
from xml.parsers.expat import model
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, ServiceAppoitment

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin']

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_ID"]


class ServiceListEncoder(ModelEncoder):
    model = ServiceAppoitment
    properties = ["vin", "customer", "technician", "appt_date","reason", "completed", "purchased","id"]
    
    encoders={
        
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_appts(request):
    if request.method == "GET":
        try:
            appoitments = ServiceAppoitment.objects.all()
            return JsonResponse(
                {"appoitments": appoitments}, encoder=ServiceListEncoder)
        except: 
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
    else:
        content = json.loads(request.body)
        print("Content**********", content)
        vin = content["vin"]

        if AutomobileVO.objects.filter(vin=vin).exists():
            content["purchased"] = True
    
        employee_ID = content["technician"]
        technician = Technician.objects.get(employee_ID=employee_ID)
        content["technician"] = technician

        appoitment = ServiceAppoitment.objects.create(**content)
        return JsonResponse(appoitment, encoder=ServiceListEncoder, safe=False)
#need to make changes in here


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_detail(request, pk):
    if request.method == "GET":
        try:
            appoitment = ServiceAppoitment.objects.get(id=pk) #thinking I may have to GET using diff parameters
            return JsonResponse(appoitment, encoder=ServiceListEncoder, safe=False)
        except ServiceAppoitment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appoitment = ServiceAppoitment.objects.get(id=pk)
            appoitment.delete()
            return JsonResponse(
                appoitment, encoder=ServiceListEncoder, safe=False)
        except ServiceAppoitment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    # else:
    #     try:
    #             content = json.loads(request.body)
    #             appoitment = ServiceAppoitment.objects.get(employee_ID=pk)

    #             props = ['name', 'employee_ID']
    #             for prop in props:
    #                 if prop in content:
    #                     setattr(technician, prop, technician[prop])
    #             technician.save()
    #             return JsonResponse(
    #                 technician,
    #                 encoder=TechnicianEncoder,
    #                 safe=False,
    #             )
    #         except Technician.DoesNotExist:
    #             response = JsonResponse(
    #                 {"message" : "Does not exist"},
    #                 status = 400,
    #             )
    #             return response




@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technician": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message" : "Could not create a new techncian"},
                status = 400,
            )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_ID=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(employee_ID=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

            #may need to make adjustments here as well 
    # else: #PUT
    #     try:
    #         content = json.loads(request.body)
    #         technician = Technician.objects.get(employee_ID=pk)

    #         props = ['name', 'employee_ID']
    #         for prop in props:
    #             if prop in content:
    #                 setattr(technician, prop, technician[prop])
    #         technician.save()
    #         return JsonResponse(
    #             technician,
    #             encoder=TechnicianEncoder,
    #             safe=False,
    #         )
    #     except Technician.DoesNotExist:
    #         response = JsonResponse(
    #             {"message" : "Does not exist"},
    #             status = 400,
    #         )
    #         return response



